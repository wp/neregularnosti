package mk.ukim.finki.wp.nepravilnosti.controllers;

import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.dto.FilterAndSortDto;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityStatus;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.IrregularityDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.ProfessorDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.RoomDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.IrregularityService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class IrregularityController {
    private final IrregularityService irregularityService;

    public IrregularityController(IrregularityService irregularityService) {
        this.irregularityService = irregularityService;
    }


    @GetMapping({"/admin-panel-reports"})
    public String getAdminPanelReportsPage(@RequestParam(required = false) String id,
                                   @RequestParam(required = false) IrregularityType type,
                                   @RequestParam(required = false) IrregularityStatus status,
                                   @RequestParam(defaultValue = "newest") String sort,
                                   @RequestParam(defaultValue = "0") int page,
                                   @RequestParam(defaultValue = "20") int size,
                                   @RequestParam(required = false) String error,
                                   HttpServletRequest request,
                                   HttpSession session,
                                   Model model) {

        Boolean isFirstLoad = (Boolean) session.getAttribute("isFirstLoad");
        if ((isFirstLoad == null || isFirstLoad)) {
            if (status==null){
                status = IrregularityStatus.UNREAD;
            }
            session.setAttribute("isFirstLoad", false);
        }

        String username = request.getRemoteUser();
        Boolean isAdmin = irregularityService.isAdministration(username);

        if(!isAdmin)
            return "redirect:/";

        Page<Irregularity> irregularitiesPage = irregularityService.filterIrregularitiesForAdmin(id, type, status, sort, username, page, size);
        model.addAttribute("IrregularityTypes", IrregularityType.values());
        model.addAttribute("irregularities", irregularitiesPage.getContent());
        model.addAttribute("statuses", IrregularityStatus.values());
        model.addAttribute("filterAndSort", new FilterAndSortDto(id, type, status, sort));
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", irregularitiesPage.getTotalPages());
        model.addAttribute("totalElements", irregularitiesPage.getTotalElements());
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("template", "admin-panel-reports.html");
        if (error != null) {
            model.addAttribute("error", error);
        }

        return "master";
    }

    @GetMapping({"/","/my-reports"})
    public String getMyReportsPage(@RequestParam(required = false) String error,
                                   HttpServletRequest request,
                                   HttpSession session,
                                   Model model) {


        String username = request.getRemoteUser();
        Boolean isAdmin = irregularityService.isAdministration(username);

        model.addAttribute("IrregularityTypes", IrregularityType.values());
        model.addAttribute("irregularities", irregularityService.findAllIrregularitiesForUser(username));
        model.addAttribute("statuses", IrregularityStatus.values());

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("template", "my-reports.html");
        if (error != null) {
            model.addAttribute("error", error);
        }

        return "master";
    }


    @GetMapping("/form-page")
    public String getFormPage(
            @RequestParam(required = false) String error,
            HttpServletRequest request,
            HttpSession session,
            Model model) {

        model.addAttribute("irregularity", new Irregularity());

        String username = request.getRemoteUser();
        Boolean isAdmin = irregularityService.isAdministration(username);

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("IrregularityTypes", IrregularityType.values());
        model.addAttribute("statuses", IrregularityStatus.values());
        if (error != null) {
            model.addAttribute("error", error);
        }
        model.addAttribute("template", "form.html");
        return "master";
    }


    @PostMapping("/new-irregularity")
    public String newIrregularity(@RequestParam String title,
                                  @RequestParam String description,
                                  @RequestParam IrregularityType type,
                                  @RequestParam String irregularityValue,
                                  Model model,
                                  HttpServletRequest req) {
        try {
            this.irregularityService.addIrregularity(title, description, type, irregularityValue, req.getRemoteUser());

        } catch (RoomDoesNotExistException | ProfessorDoesNotExistException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/?error=" + e.getMessage();
        } catch (MessagingException e) {
            model.addAttribute("error", "Email sending failed.");
            return "redirect:/?error=Email-sending-failed."; //TODO: what to do if email fails
        }
        return "redirect:/";
    }

    @GetMapping("irregularity/edit-form/{id}")
    public String getEditForm(
            @PathVariable String id,
            @RequestParam(required = false) String error,
            HttpServletRequest request,
            HttpSession session,
            Model model
    ) {
        Irregularity irregularity = irregularityService.getIrregularityById(id);
//        if (irregularity.getStatus() != Status.UNREAD) {
//            return "redirect:/";
//        }
        String username = request.getRemoteUser();
        Boolean isAdmin = irregularityService.isAdministration(username);

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("user", username);
        model.addAttribute("irregularity", irregularity);
        model.addAttribute("type", irregularity.getType());
        model.addAttribute("selectedValue", irregularity.getIrregularityValue());
        model.addAttribute("IrregularityTypes", IrregularityType.values());
        model.addAttribute("statuses", IrregularityStatus.values());
        if (error != null) {
            model.addAttribute("error", error);
        }
        model.addAttribute("template", "formEdit.html");
        return "master";
    }

    @PostMapping("irregularity/edit/{id}")
    public String editIrregularity(@PathVariable String id,
                                   @RequestParam String title,
                                   @RequestParam String description,
                                   @RequestParam IrregularityType type,
                                   @RequestParam String irregularityValue) {
        try {
            this.irregularityService.editIrregularity(id, title, description, type, irregularityValue);
        } catch (IrregularityDoesNotExistException e) {
            return "redirect:/?error=" + e.getMessage();
        }
        return "redirect:/";
    }

    @PostMapping("irregularity/delete/{id}")
    public String deleteIrregularity(@PathVariable String id) {
        try {
            this.irregularityService.deleteIrregularity(id);
        } catch (IrregularityDoesNotExistException e) {
            return "redirect:/?error=" + e.getMessage();
        }

        return "redirect:/";
    }

    @GetMapping("irregularity/thread/{id}")
    public String getIrregularityThread(@PathVariable String id,
                                        Model model,
                                        HttpServletRequest req) {
        try {
            var irregularityThread = irregularityService.getIrregularityById(id);

            irregularityService.changeInitialStatus(id, IrregularityStatus.READ, req.getRemoteUser());

            var comments = irregularityThread.getStaffComment();

            String username = req.getRemoteUser();
            boolean isAdmin = irregularityService.isAdministration(username);

            model.addAttribute("isAdmin", isAdmin);
            model.addAttribute("thread", irregularityThread);
            model.addAttribute("threadComments", comments);
            model.addAttribute("user", username);
            model.addAttribute("template", "thread.html");
        } catch (IrregularityDoesNotExistException e) {
            return "redirect:/?error=" + e.getMessage();
        }


        return "master";
    }

    @PostMapping("irregularity/thread/{id}/add-comment")
    public String addStaffComment(@PathVariable String id,
                                  @RequestParam String staffComment,
                                  HttpServletRequest req) {

        irregularityService.addCommentToThread(id, staffComment, req.getRemoteUser());

        return String.format("redirect:/irregularity/thread/%s", id);
    }

    @PostMapping("irregularity/thread/{id}/change-status")
    public String changeThreadStatus(@PathVariable String id,
                                     HttpServletRequest req) {
        try {
            irregularityService.changeStatus(id, req.getRemoteUser());
        } catch (MessagingException e) {
            return "redirect:/?error=Email-sending-failed."; //TODO: what to do if email fails
        }
        return String.format("redirect:/irregularity/thread/%s", id);
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest req,
                         Model model) {
        model.addAttribute("template", "anonymous.html");
        model.addAttribute("isAdmin", false);
        return "master";
    }


    @GetMapping("/welcome")
    public String welcome(HttpServletRequest req,
                         Model model) {
        model.addAttribute("template", "anonymous.html");
        model.addAttribute("isAdmin", false);
        return "master";
    }
}


