package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.Room;

import java.util.List;

public interface RoomService {
    List<String> getRoomNames();
    Room getRoomByName(String name);
}
