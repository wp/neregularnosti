package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.Subject;
import mk.ukim.finki.wp.nepravilnosti.models.dto.SubjectDto;

import java.util.List;

public interface SubjectService {
    List<SubjectDto> getAllSubjectDto();
    Subject getSubjectById(String id);
}
