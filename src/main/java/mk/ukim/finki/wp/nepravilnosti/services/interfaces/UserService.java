package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.User;

import java.util.List;

public interface UserService {
    User findByUserName(String userName);
    List<User> findAdministration();
}