package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender emailSender;

    @Value("${app.base-url}")
    private String baseUrl;

    @Value("${app.notification-emails}")
    private String[] notificationEmails;

    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendNewIrregularityEmail(String to, String irregularityId) throws MessagingException {
        String subject = "Потврда за креирана нова нерегуларност";

        String message = "<!DOCTYPE html>"
                + "<html lang=\"mk\">"
                + "<head>"
                + "<meta charset=\"UTF-8\">"
                + "<style>"
                + "body { font-family: Arial, sans-serif; line-height: 1.6; background-color: #f9f9f9; padding: 20px; }"
                + ".container { max-width: 600px; margin: auto; background: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); }"
                + "h3 { color: #2c3e50; }"
                + "p { color: #333; }"
                + ".button { display: inline-block; background-color: #4CAF50; color: white; padding: 12px 20px; text-decoration: none; border-radius: 4px; font-size: 16px; font-weight: bold; }"
                + ".footer { font-size: 0.9em; color: #666; margin-top: 20px; text-align: center; }"
                + ".footer a { color: #4CAF50; text-decoration: none; }"
                + "</style>"
                + "</head>"
                + "<body>"
                + "<div class='container'>"
                + "<h3>Почитуван/а,</h3>"
                + "<p>Вашата пријава за нерегуларност <strong>(ID: " + irregularityId + ")</strong> е успешно регистрирана.</p>"
                + "<p>Можете да ја следите состојбата на вашата пријава преку следниов линк:</p>"
                + "<p style='text-align: center;'>"
                + "<a href='" + baseUrl + "/irregularity/thread/" + irregularityId + "' class='button'>Преглед на нерегуларноста</a>"
                + "</p>"
                + "<hr style='border: 0.5px solid #e0e0e0;'>"
                + "<div class='footer'>"
                + "<p>⚠️ <strong>Важно:</strong> Овој мејл е автоматски генериран. Ве молиме не одговарајте на него.</p>"
                + "<p>За дополнителни прашања, контактирајте нè на <a href='mailto:irregularities@finki.ukim.mk'>irregularities@finki.ukim.mk</a></p>"
                + "</div>"
                + "</div>"
                + "</body>"
                + "</html>";

        sendEmail(to, subject, message);
    }


    @Override
    public void sendStatusChangedEmail(String to, String irregularityId) throws MessagingException {
        String subject = "Решена пријава за нерегуларност";

        String message = "<!DOCTYPE html>"
                + "<html lang=\"mk\">"
                + "<head>"
                + "<meta charset=\"UTF-8\">"
                + "<style>"
                + "body { font-family: Arial, sans-serif; line-height: 1.6; background-color: #f9f9f9; padding: 20px; }"
                + ".container { max-width: 600px; margin: auto; background: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); }"
                + "h3 { color: #2c3e50; }"
                + "p { color: #333; }"
                + ".button { display: inline-block; background-color: #4CAF50; color: white; padding: 12px 20px; text-decoration: none; border-radius: 4px; font-size: 16px; font-weight: bold; }"
                + ".footer { font-size: 0.9em; color: #666; margin-top: 20px; text-align: center; }"
                + ".footer a { color: #4CAF50; text-decoration: none; }"
                + "</style>"
                + "</head>"
                + "<body>"
                + "<div class='container'>"
                + "<h3>Почитуван/а,</h3>"
                + "<p>Вашата пријава за нерегуларност <strong>(ID: " + irregularityId + ")</strong> е успешно разрешена.</p>"
                + "<p>Можете да ги погледнете деталите преку следниов линк:</p>"
                + "<p style='text-align: center;'>"
                + "<a href='" + baseUrl + "/irregularity/thread/" + irregularityId + "' class='button'>Преглед на решената нерегуларност</a>"
                + "</p>"
                + "<hr style='border: 0.5px solid #e0e0e0;'>"
                + "<div class='footer'>"
                + "<p>⚠️ <strong>Важно:</strong> Овој мејл е автоматски генериран. Ве молиме не одговарајте на него.</p>"
                + "<p>За дополнителни прашања, контактирајте нè на <a href='mailto:irregularities@finki.ukim.mk'>irregularities@finki.ukim.mk</a></p>"
                + "</div>"
                + "</div>"
                + "</body>"
                + "</html>";

        sendEmail(to, subject, message);
    }


    @Override
    public void sendUnresolvedIrregularityEmail(String message) throws MessagingException {
        String subject = "Нерешени нерегуларности оваа недела";

        for (String email : notificationEmails) {
            sendEmail(email, subject, message);
        }
    }

    public void sendEmail(String to, String subject, String messageText) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setTo(to);
        helper.setFrom("noreply@neregularnosti.finki.ukim.mk");
        helper.setSubject(subject);
        helper.setText(messageText, true);
        emailSender.send(message);
    }
}