package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.Student;
import mk.ukim.finki.wp.nepravilnosti.models.dto.StudentDto;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.StudentNotExistException;
import mk.ukim.finki.wp.nepravilnosti.repositories.StudentRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student getStudentByIndex(String index) {
        return studentRepository.findById(index).orElseThrow(StudentNotExistException::new);
    }

    @Override
    public List<StudentDto> getAllStudentsDto() {
        return studentRepository.findAll()
                .stream()
                .map(student -> new StudentDto(student.getIndex(), student.getName()))
                .collect(Collectors.toList());
    }
}
