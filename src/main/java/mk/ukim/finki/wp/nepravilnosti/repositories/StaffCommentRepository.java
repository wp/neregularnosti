package mk.ukim.finki.wp.nepravilnosti.repositories;

import mk.ukim.finki.wp.nepravilnosti.models.IrregularityComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffCommentRepository extends JpaRepository<IrregularityComment, Long> {
}
