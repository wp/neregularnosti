package mk.ukim.finki.wp.nepravilnosti.models.exceptions;

public class ProfessorDoesNotExistException extends RuntimeException {
    public ProfessorDoesNotExistException() {
        super("The professor you entered does not exist");
    }
}
