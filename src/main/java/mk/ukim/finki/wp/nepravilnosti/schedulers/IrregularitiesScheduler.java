package mk.ukim.finki.wp.nepravilnosti.schedulers;

import jakarta.mail.MessagingException;
import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.EmailService;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.IrregularityService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableScheduling
public class IrregularitiesScheduler {

    private final IrregularityService irregularityService;
    private final EmailService emailService;

    @Value("${app.base-url}")
    private String baseUrl;


    public IrregularitiesScheduler(IrregularityService irregularityService, EmailService emailService) {
        this.irregularityService = irregularityService;
        this.emailService = emailService;
    }

    @Scheduled(cron = "0 0 12 * * *", zone = "Europe/Skopje")
    public void checkIrregularitiesOlderThanWeek() {
        List<Irregularity> notResolvedIrregularities = irregularityService.getUnresolvedIrregularitiesOlderThanWeek();

        if (notResolvedIrregularities.isEmpty()) {
            return;
        }

        StringBuilder sb = new StringBuilder();


        sb.append("<!DOCTYPE html>")
                .append("<html lang=\"mk\">")
                .append("<head>")
                .append("<meta charset=\"UTF-8\">")
                .append("<style>")
                .append("body { font-family: Arial, sans-serif; line-height: 1.6; background-color: #f9f9f9; padding: 20px; }")
                .append(".container { max-width: 600px; margin: auto; background: #ffffff; padding: 20px; border-radius: 5px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); }")
                .append("h3 { color: #2c3e50; }")
                .append("p { color: #333; }")
                .append(".irregularity-list { padding: 0; list-style-type: none; }")
                .append(".irregularity-list li { margin: 8px 0; }")
                .append(".irregularity-list a { color: #4CAF50; text-decoration: none; font-weight: bold; }")
                .append(".footer { font-size: 0.9em; color: #666; margin-top: 20px; text-align: center; }")
                .append(".footer a { color: #4CAF50; text-decoration: none; }")
                .append("</style>")
                .append("</head>")
                .append("<body>")
                .append("<div class='container'>")
                .append("<h3>Почитуван/а,</h3>")
                .append("<p>Ова е листа на нерешени пријави за нерегуларности постари од една недела:</p>")
                .append("<ul class='irregularity-list'>");

        int index = 1;
        for (Irregularity irregularity : notResolvedIrregularities) {
            sb.append("<li>")
                    .append(index)
                    .append(". <a href=\"").append(baseUrl).append("/irregularity/thread/").append(irregularity.getId()).append("\">")
                    .append(irregularity.getTitle())
                    .append("</a></li>");
            index++;
        }

        sb.append("</ul>")
                .append("<hr style='border: 0.5px solid #e0e0e0;'>")
                .append("<div class='footer'>")
                .append("<p>⚠️ <strong>Важно:</strong> Овој мејл е автоматски генериран. Ве молиме не одговарајте на него.</p>")
                .append("<p>За дополнителни прашања, контактирајте нè на <a href='mailto:irregularities@finki.ukim.mk'>irregularities@finki.ukim.mk</a></p>")
                .append("</div>")
                .append("</div>")
                .append("</body>")
                .append("</html>");

        try {
            emailService.sendUnresolvedIrregularityEmail(sb.toString());
        } catch (MessagingException e) {
            System.out.println("Problem sending email: " + e.getMessage());
        }
    }


}
