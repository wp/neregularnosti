package mk.ukim.finki.wp.nepravilnosti.models.exceptions;

public class IrregularityDoesNotExistException extends RuntimeException{
    public IrregularityDoesNotExistException() {
        super("The irregularity does not exist");
    }
}
