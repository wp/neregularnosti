package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.Professor;
import mk.ukim.finki.wp.nepravilnosti.models.dto.ProfessorDto;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.ProfessorDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.repositories.ProfessorRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.ProfessorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;

    public ProfessorServiceImpl(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    @Override
    public List<ProfessorDto> getProfessorsDto() {
        return professorRepository.findAll().stream()
                .map(professor -> new ProfessorDto(professor.getId(), professor.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public Professor getProfessorById(String id) {
        return this.professorRepository.findById(id).orElseThrow(ProfessorDoesNotExistException::new);
    }
}
