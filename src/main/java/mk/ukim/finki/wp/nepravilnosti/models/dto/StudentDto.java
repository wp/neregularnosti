package mk.ukim.finki.wp.nepravilnosti.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {
    String id; //index
    String name;

    @Override
    public String toString() {
        return id + ' ' + name;
    }
}
