package mk.ukim.finki.wp.nepravilnosti.models.exceptions;

public class StudentNotExistException extends RuntimeException{
    public StudentNotExistException() {
        super("The student you entered does not exist");
    }
}
