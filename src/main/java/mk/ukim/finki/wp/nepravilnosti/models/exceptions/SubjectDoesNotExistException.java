package mk.ukim.finki.wp.nepravilnosti.models.exceptions;

public class SubjectDoesNotExistException extends RuntimeException{
    public SubjectDoesNotExistException() {
        super("The subject does not exist");
    }
}
