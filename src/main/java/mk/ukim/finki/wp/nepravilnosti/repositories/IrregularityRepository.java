package mk.ukim.finki.wp.nepravilnosti.repositories;

import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;


@Repository
public interface IrregularityRepository extends JpaRepository<Irregularity, String>, JpaSpecificationExecutor<Irregularity> {
    List<Irregularity> findByStatusInAndDateBefore(List<IrregularityStatus> status, LocalDateTime date);

}
