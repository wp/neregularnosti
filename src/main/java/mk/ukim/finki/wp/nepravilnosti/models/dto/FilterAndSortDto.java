package mk.ukim.finki.wp.nepravilnosti.models.dto;

import lombok.Getter;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityStatus;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;

@Getter
public class FilterAndSortDto {
    private String id;
    private IrregularityType type;
    private IrregularityStatus status;
    private String sort;

    public FilterAndSortDto(String id, IrregularityType type, IrregularityStatus status, String sort) {
        this.id = id;
        this.type = type;
        this.sort = sort;
        this.status = status;
    }
}
