package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;
import mk.ukim.finki.wp.nepravilnosti.repositories.IrregularityRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.AnalyticsService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.*;

@Service
public class AnalyticsServiceImpl implements AnalyticsService {
    private final IrregularityRepository irregularityRepository;

    public AnalyticsServiceImpl(IrregularityRepository irregularityRepository) {
        this.irregularityRepository = irregularityRepository;
    }

    public Map<IrregularityType, Map<Irregularity, Long>> getIrregularityCounts() {
        List<Irregularity> irregularities = irregularityRepository.findAll();

        Map<IrregularityType, Map<String, List<Irregularity>>> groupedIrregularities = irregularities.stream()
                .collect(Collectors.groupingBy(
                        Irregularity::getType,
                        Collectors.groupingBy(
                                this::getIrregularityOutsideKey
                        )
                ));

        return groupedIrregularities.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().entrySet().stream()
                                .collect(Collectors.toMap(
                                        e -> e.getValue().get(0),
                                        e -> (long) e.getValue().size(),
                                        (v1, v2) -> v1,
                                        LinkedHashMap::new
                                ))
                ));
    }


    private String getIrregularityOutsideKey(Irregularity irregularity) {
        return switch (irregularity.getType()) {
            case PROFESSOR -> irregularity.getProfessor() != null ? irregularity.getProfessor().getId() : null;
            case ROOM -> irregularity.getRoom() != null ? irregularity.getRoom().getName() : null;
            case SUBJECT -> irregularity.getSubject() != null ? irregularity.getSubject().getId() : null;
            case STUDENT -> irregularity.getStudent() != null ? irregularity.getStudent().getIndex() : null;
            case ADMINISTRATION -> irregularity.getAdministrativeStaff() != null ? irregularity.getAdministrativeStaff().getId() : null;
            case GENERAL -> irregularity.getGeneralIrregularity();
        };
    }

}