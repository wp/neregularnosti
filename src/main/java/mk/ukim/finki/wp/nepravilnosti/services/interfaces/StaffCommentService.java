package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.IrregularityComment;

public interface StaffCommentService {
    void saveStaffComment(IrregularityComment comment);
}
