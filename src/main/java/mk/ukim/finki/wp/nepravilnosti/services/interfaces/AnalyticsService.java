package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;

import java.util.Map;

public interface AnalyticsService {
    Map<IrregularityType, Map<Irregularity, Long>> getIrregularityCounts();
}
