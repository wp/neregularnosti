package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.Room;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.RoomDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.repositories.RoomRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.RoomService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {
    private final RoomRepository roomRepository;

    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public List<String> getRoomNames() {
        return roomRepository.getRoomNames();
    }

    @Override
    public Room getRoomByName(String name) {
        return roomRepository.findById(name).orElseThrow(RoomDoesNotExistException::new);
    }


}
