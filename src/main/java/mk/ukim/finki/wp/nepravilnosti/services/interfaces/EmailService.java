package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import jakarta.mail.MessagingException;

public interface EmailService {
    void sendNewIrregularityEmail(String to, String irregularityId) throws MessagingException;
    void sendStatusChangedEmail(String to, String irregularityId) throws MessagingException;
    void sendUnresolvedIrregularityEmail(String message) throws MessagingException;
}
