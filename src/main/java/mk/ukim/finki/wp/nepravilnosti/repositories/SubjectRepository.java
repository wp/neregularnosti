package mk.ukim.finki.wp.nepravilnosti.repositories;

import mk.ukim.finki.wp.nepravilnosti.models.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
}
