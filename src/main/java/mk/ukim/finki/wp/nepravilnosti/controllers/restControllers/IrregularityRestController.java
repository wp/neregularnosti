package mk.ukim.finki.wp.nepravilnosti.controllers.restControllers;

import mk.ukim.finki.wp.nepravilnosti.models.User;
import mk.ukim.finki.wp.nepravilnosti.models.dto.ProfessorDto;
import mk.ukim.finki.wp.nepravilnosti.models.dto.StudentDto;
import mk.ukim.finki.wp.nepravilnosti.models.dto.SubjectDto;
import mk.ukim.finki.wp.nepravilnosti.schedulers.IrregularitiesScheduler;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class IrregularityRestController {
    private final ProfessorService professorService;
    private final RoomService roomService;
    private final SubjectService subjectService;
    private final StudentService studentService;
    private final UserService userService;
    private final IrregularitiesScheduler scheduler;

    public IrregularityRestController(ProfessorService professorService, RoomService roomService, SubjectService subjectService, StudentService studentService, UserService userService, IrregularitiesScheduler scheduler) {
        this.professorService = professorService;
        this.roomService = roomService;
        this.subjectService = subjectService;
        this.studentService = studentService;
        this.userService = userService;
        this.scheduler = scheduler;
    }

    @GetMapping("/professors")
    public List<ProfessorDto> getProfessors() {
        return professorService.getProfessorsDto();
    }

    @GetMapping("/rooms")
    public List<String> getRooms() {
        return roomService.getRoomNames();
    }

    @GetMapping("/subjects")
    public List<SubjectDto> getSubjects() {
        return subjectService.getAllSubjectDto();
    }

    @GetMapping("/students")
    public List<StudentDto> getStudents() {
        return studentService.getAllStudentsDto();
    }

    @GetMapping("/administrations")
    public List<User> getAdministrators() {
        return userService.findAdministration();
    }

    @GetMapping("/generals")
    public List<String> getGenerals() {
        return List.of("Друго");
    }

    @GetMapping("/scheduler/trigger")
    public ResponseEntity<String> triggerScheduler() {
        scheduler.checkIrregularitiesOlderThanWeek();
        return ResponseEntity.ok("Scheduler triggered manually!");
    }

}
