package mk.ukim.finki.wp.nepravilnosti.models.enums;

public enum AppRole {
    ADMIN, PROFESSOR, GUEST;


    public String roleName() {
        return "ROLE_" + this.name();
    }
}
