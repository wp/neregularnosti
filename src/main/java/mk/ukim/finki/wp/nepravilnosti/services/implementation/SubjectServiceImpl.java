package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.Subject;
import mk.ukim.finki.wp.nepravilnosti.models.dto.SubjectDto;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.SubjectDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.repositories.SubjectRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.SubjectService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<SubjectDto> getAllSubjectDto() {
        return subjectRepository.findAll().stream()
                .map(subject -> new SubjectDto(subject.getId(), subject.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public Subject getSubjectById(String id) {
        return this.subjectRepository.findById(id).orElseThrow(SubjectDoesNotExistException::new);
    }
}
