package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.Student;
import mk.ukim.finki.wp.nepravilnosti.models.dto.StudentDto;

import java.util.List;

public interface StudentService {
    Student getStudentByIndex(String index);
    List<StudentDto> getAllStudentsDto();
}
