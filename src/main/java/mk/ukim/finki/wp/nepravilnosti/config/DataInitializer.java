package mk.ukim.finki.wp.nepravilnosti.config;

import jakarta.annotation.PostConstruct;
import mk.ukim.finki.wp.nepravilnosti.models.Student;
import mk.ukim.finki.wp.nepravilnosti.repositories.StudentRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataInitializer {
    private final StudentRepository studentRepository;

    public DataInitializer(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @PostConstruct
    public void init() {
        if (studentRepository.findAll().isEmpty()) {
            List<Student> students = new ArrayList<>();

            for (int i = 0; i < 10; i++) {
                Student student = new Student();
                student.setIndex("student" + i);
                student.setEmail("student" + i + "@example.com");
                student.setName("Student " + i);
                student.setLastName("LastName " + i);
                student.setParentName("Parent " + i);

                students.add(student);
            }

            studentRepository.saveAll(students);
        }
    }


}
