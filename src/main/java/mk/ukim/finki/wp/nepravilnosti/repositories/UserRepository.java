package mk.ukim.finki.wp.nepravilnosti.repositories;

import mk.ukim.finki.wp.nepravilnosti.models.User;
import mk.ukim.finki.wp.nepravilnosti.models.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    List<User> findByRole(UserRole role);

}
