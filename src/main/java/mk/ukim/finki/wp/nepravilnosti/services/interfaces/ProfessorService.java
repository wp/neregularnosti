package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import mk.ukim.finki.wp.nepravilnosti.models.Professor;
import mk.ukim.finki.wp.nepravilnosti.models.dto.ProfessorDto;

import java.util.List;

public interface ProfessorService {
    List<ProfessorDto> getProfessorsDto();
    Professor getProfessorById(String id);
}
