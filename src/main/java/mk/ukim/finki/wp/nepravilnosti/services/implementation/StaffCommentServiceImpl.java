package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.IrregularityComment;
import mk.ukim.finki.wp.nepravilnosti.repositories.StaffCommentRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.StaffCommentService;
import org.springframework.stereotype.Service;

@Service
public class StaffCommentServiceImpl implements StaffCommentService {
    private final StaffCommentRepository staffCommentRepository;

    public StaffCommentServiceImpl(StaffCommentRepository staffCommentRepository) {
        this.staffCommentRepository = staffCommentRepository;
    }

    @Override
    public void saveStaffComment(IrregularityComment comment) {
        staffCommentRepository.save(comment);
    }
}
