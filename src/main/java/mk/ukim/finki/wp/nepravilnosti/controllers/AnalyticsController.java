package mk.ukim.finki.wp.nepravilnosti.controllers;

import jakarta.servlet.http.HttpServletRequest;
import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.AnalyticsService;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.IrregularityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class AnalyticsController {
    private final AnalyticsService analyticsService;
    private final IrregularityService irregularityService;

    public AnalyticsController(AnalyticsService analyticsService,
                               IrregularityService irregularityService) {
        this.analyticsService = analyticsService;
        this.irregularityService = irregularityService;
    }

    @GetMapping("/analytics")
    public String showIrregularityStats(Model model,
                                        HttpServletRequest req) {

        String username = req.getRemoteUser();
        Boolean isAdmin = irregularityService.isAdministration(username);

        Map<IrregularityType, Map<Irregularity, Long>> irregularityCounts = analyticsService.getIrregularityCounts();

        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("irregularityCounts", irregularityCounts);
        model.addAttribute("template", "analytics.html");
        return "master";
    }
}
