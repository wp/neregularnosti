package mk.ukim.finki.wp.nepravilnosti.models.exceptions;

public class RoomDoesNotExistException extends RuntimeException{
    public RoomDoesNotExistException() {
        super("The subject does not exist");
    }
}
