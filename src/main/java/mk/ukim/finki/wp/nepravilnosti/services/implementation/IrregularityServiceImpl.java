package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import jakarta.mail.MessagingException;
import mk.ukim.finki.wp.nepravilnosti.models.*;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityStatus;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;
import mk.ukim.finki.wp.nepravilnosti.models.enums.UserRole;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.IrregularityDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.models.specifications.IrregularitySpecifications;
import mk.ukim.finki.wp.nepravilnosti.repositories.IrregularityRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.jpa.domain.Specification;


import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IrregularityServiceImpl implements IrregularityService {
    private final IrregularityRepository irregularityRepository;
    private final ProfessorService professorService;
    private final RoomService roomService;
    private final SubjectService subjectService;
    private final StudentService studentService;
    private final UserService userService;
    private final StaffCommentService staffCommentService;
    private final EmailService emailService;
    public IrregularityServiceImpl(IrregularityRepository irregularityRepository, ProfessorService professorService, RoomService roomService, SubjectService subjectService, StudentService studentService, UserService userService, StaffCommentService staffCommentService, EmailService emailService) {
        this.irregularityRepository = irregularityRepository;
        this.professorService = professorService;
        this.roomService = roomService;
        this.subjectService = subjectService;
        this.studentService = studentService;
        this.userService = userService;
        this.staffCommentService = staffCommentService;
        this.emailService = emailService;
    }

    @Override
    public Page<Irregularity> filterIrregularities(String id, IrregularityType type, IrregularityStatus status, String sort, String username, int page, int size) {
        Specification<Irregularity> spec = this.buildIrregularitySpecification(id, type, status);

        Sort sortOrder = generateSortOrder(sort);

        PageRequest pageRequest = PageRequest.of(page, size, sortOrder);

        User user = userService.findByUserName(username);
        Page<Irregularity> irregularitiesPage = irregularityRepository.findAll(spec, pageRequest);
        List<Irregularity> filteredIrregularities = irregularitiesPage
            .stream()
            .filter(irregularity -> irregularity.getUser().equals(user))
            .collect(Collectors.toList());

        return new PageImpl<>(filteredIrregularities, pageRequest, filteredIrregularities.size());
    }

    @Override
    public Page<Irregularity> filterIrregularitiesForAdmin(String id, IrregularityType type, IrregularityStatus status, String sort, String username, int page, int size) {
        Specification<Irregularity> spec = this.buildIrregularitySpecification(id, type, status);

        Sort sortOrder = generateSortOrder(sort);

        PageRequest pageRequest = PageRequest.of(page, size, sortOrder);

        User user = userService.findByUserName(username);
        Page<Irregularity> irregularitiesPage = irregularityRepository.findAll(spec, pageRequest);
        if (!checkRoleForAllIrregularities(user.getRole())) {
            List<Irregularity> filteredIrregularities = irregularitiesPage
                    .stream()
                    .filter(irregularity -> irregularity.getUser().equals(user))
                    .collect(Collectors.toList());

            return new PageImpl<>(filteredIrregularities, pageRequest, filteredIrregularities.size());
        }

        return irregularitiesPage;
    }

    @Override
    public Irregularity getIrregularityById(String id) throws IrregularityDoesNotExistException {
        return this.irregularityRepository.findById(id).orElseThrow(IrregularityDoesNotExistException::new);
    }

    @Override
    public void addIrregularity(String title, String description, IrregularityType type, String outsideKey, String username) throws MessagingException {
        if (type == null) {
            throw new IllegalArgumentException("Type of irregularity cannot be null");
        }
        User user = userService.findByUserName(username);

        Irregularity irregularity = buildIrregularity(title, description, type, outsideKey, user);
        this.irregularityRepository.save(irregularity);


        emailService.sendNewIrregularityEmail(user.getEmail(), irregularity.getId());
    }

    @Override
    public void editIrregularity(String id, String title, String description, IrregularityType type, String outsideKye) {
        Irregularity irregularity = getIrregularityById(id);

        irregularity.setTitle(title);
        irregularity.setDescription(description);
        irregularity.setType(type);

        switch (type) {
            case PROFESSOR:
                Professor professor = professorService.getProfessorById(outsideKye);
                irregularity.setProfessor(professor);
                break;
            case ROOM:
                Room room = roomService.getRoomByName(outsideKye);
                irregularity.setRoom(room);
                break;
            case SUBJECT:
                Subject subject = subjectService.getSubjectById(outsideKye);
                irregularity.setSubject(subject);
                break;
            case STUDENT:
                Student student = studentService.getStudentByIndex(outsideKye);
                irregularity.setStudent(student);
                break;
            case ADMINISTRATION:
                User user = userService.findByUserName(outsideKye);
                irregularity.setAdministrativeStaff(user);
                break;
            case GENERAL:
                irregularity.setGeneralIrregularity(outsideKye);
                break;
        }

        irregularityRepository.save(irregularity);
    }
    @Override
    public void deleteIrregularity(String id) {
        Irregularity irregularity = this.getIrregularityById(id);

        this.irregularityRepository.delete(irregularity);
    }

    @Override
    public void changeInitialStatus(String id, IrregularityStatus status, String username) {
        Irregularity irregularity = this.getIrregularityById(id);
        User user = userService.findByUserName(username);

        if(irregularity.getStatus() != IrregularityStatus.UNREAD) {
            return;
        }

        switch(user.getRole()) {
            case ACADEMIC_AFFAIR_VICE_DEAN:
            case STUDENT_ADMINISTRATION_MANAGER:
            case ADMINISTRATION_MANAGER:
            case DEAN:
            case SCIENCE_AND_COOPERATION_VICE_DEAN:
            case FINANCES_VICE_DEAN:
                irregularity.setStatus(status);
                break;
            default:
                break;
        }

        this.irregularityRepository.save(irregularity);
    }

    @Override
    public void addCommentToThread(String id, String comment, String username) {
        Irregularity irregularity = this.getIrregularityById(id);
        User user = userService.findByUserName(username);


        var staffComment = new IrregularityComment();
        staffComment.setComment(comment);
        staffComment.setStaffUser(user);
        staffComment.setDateAnswered(new Date());

        staffCommentService.saveStaffComment(staffComment);

        assert irregularity.getStaffComment() != null;
        irregularity.getStaffComment().add(staffComment);

        irregularityRepository.save(irregularity);
    }

    @Override
    public void changeStatus(String id, String username) throws MessagingException {
        Irregularity irregularity = this.getIrregularityById(id);
        User user = userService.findByUserName(username);

        switch(user.getRole()) {
            case ACADEMIC_AFFAIR_VICE_DEAN:
            case STUDENT_ADMINISTRATION_MANAGER:
            case ADMINISTRATION_MANAGER:
            case DEAN:
            case SCIENCE_AND_COOPERATION_VICE_DEAN:
            case FINANCES_VICE_DEAN:
                irregularity.setMarkedBy(user.getName());
                irregularity.setMarkedOn(LocalDateTime.now());
                irregularity.setStatus(IrregularityStatus.DONE);
                irregularity.setLocked(true);
                break;
            default:
                break;
        }

        this.irregularityRepository.save(irregularity);

        emailService.sendStatusChangedEmail(user.getEmail(), irregularity.getId());
    }

    @Override
    public boolean isAdministration(String username) {
        User user = userService.findByUserName(username);
        boolean isAdmin = false;

        switch(user.getRole()) {
            case ACADEMIC_AFFAIR_VICE_DEAN:
            case STUDENT_ADMINISTRATION_MANAGER:
            case ADMINISTRATION_MANAGER:
            case DEAN:
            case SCIENCE_AND_COOPERATION_VICE_DEAN:
            case FINANCES_VICE_DEAN:
                isAdmin = true;
                break;
            default:
                break;
        }

        return isAdmin;
    }

    @Override
    public List<Irregularity> getUnresolvedIrregularitiesOlderThanWeek() {
        LocalDateTime now = LocalDateTime.now();
        List<IrregularityStatus> statuses = List.of(IrregularityStatus.UNREAD, IrregularityStatus.READ);

        return irregularityRepository.findAll().stream()
                .filter(irregularity -> statuses.contains(irregularity.getStatus()))
                .filter(irregularity -> {
                    long weeksSinceCreation = java.time.temporal.ChronoUnit.WEEKS.between(irregularity.getDate(), now);
                    return weeksSinceCreation >= 1 && now.toLocalDate().equals(irregularity.getDate().plusWeeks(weeksSinceCreation).toLocalDate());
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Irregularity> findAllIrregularitiesForUser(String username) {

        List<Irregularity> irregularities = irregularityRepository.findAll()
                                                                    .stream()
                                                                    .filter( irregularity -> Objects.equals(irregularity.getUser().getId(), username))
                                                                    .collect(Collectors.toList());


        return irregularities;
    }


    private Irregularity buildIrregularity(String title, String description, IrregularityType type, String outsideKey, User user) {
        Irregularity.IrregularityBuilder builder = Irregularity.builder()
                .title(title)
                .description(description)
                .type(type)
                .status(IrregularityStatus.UNREAD)
                .user(user);

        switch (type) {
            case PROFESSOR -> {
                Professor professor = professorService.getProfessorById(outsideKey);
                builder.professor(professor);
            }
            case ROOM -> {
                Room room = roomService.getRoomByName(outsideKey);
                builder.room(room);
            }
            case SUBJECT -> {
                Subject subject = subjectService.getSubjectById(outsideKey);
                builder.subject(subject);
            }
            case STUDENT -> {
                Student student = studentService.getStudentByIndex(outsideKey);
                builder.student(student);
            }
            case ADMINISTRATION -> {
                User administrativePerson = userService.findByUserName(outsideKey);
                builder.administrativeStaff(administrativePerson);
            }
            case GENERAL -> {
                builder.generalIrregularity(outsideKey);
            }
            default -> throw new IllegalArgumentException("Invalid type: " + type);
        }
        return builder.build();
    }

    private Specification<Irregularity> buildIrregularitySpecification(String id, IrregularityType type, IrregularityStatus status){
        Specification<Irregularity> spec = Specification.where(null);

        if (id != null && !id.isEmpty()) {
            spec = spec.and(IrregularitySpecifications.withId(id));
        }

        if (type != null) {
            spec = spec.and(IrregularitySpecifications.withType(type));
        }

        if (status != null) {
            spec = spec.and(IrregularitySpecifications.withStatus(status));
        }

        return spec;
    }

    private Sort generateSortOrder(String sort) {
        Sort sortOrder;

        if (sort.equals("newest")) {
            sortOrder = Sort.by(Sort.Order.desc("date"));
        } else {
            sortOrder = Sort.by(Sort.Order.asc("date"));
        }

        return sortOrder;
    }




    private boolean checkRoleForAllIrregularities(UserRole role) {
        EnumSet<UserRole> validRoles = EnumSet.of(
                UserRole.ACADEMIC_AFFAIR_VICE_DEAN,
                UserRole.STUDENT_ADMINISTRATION_MANAGER,
                UserRole.ADMINISTRATION_MANAGER,
                UserRole.DEAN,
                UserRole.SCIENCE_AND_COOPERATION_VICE_DEAN,
                UserRole.FINANCES_VICE_DEAN);

        return validRoles.contains(role);
    }

}
