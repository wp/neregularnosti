package mk.ukim.finki.wp.nepravilnosti.services.implementation;

import mk.ukim.finki.wp.nepravilnosti.models.User;
import mk.ukim.finki.wp.nepravilnosti.models.enums.UserRole;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.UserDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.repositories.UserRepository;
import mk.ukim.finki.wp.nepravilnosti.services.interfaces.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByUserName(String userName) {
        return this.userRepository.findById(userName).orElseThrow(UserDoesNotExistException::new);
    }

    @Override
    public List<User> findAdministration() {
        List<UserRole> administrationRoles = List.of(
                UserRole.STUDENT_ADMINISTRATION,
                UserRole.STUDENT_ADMINISTRATION_MANAGER,
                UserRole.FINANCE_ADMINISTRATION,
                UserRole.FINANCE_ADMINISTRATION_MANAGER,
                UserRole.LEGAL_ADMINISTRATION,
                UserRole.ARCHIVE_ADMINISTRATION,
                UserRole.ADMINISTRATION_MANAGER
        );

        List<User> administrativeUsers = new ArrayList<>();

        for (UserRole role : administrationRoles) {
            administrativeUsers.addAll(this.userRepository.findByRole(role));
        }

        return administrativeUsers;
    }
}
