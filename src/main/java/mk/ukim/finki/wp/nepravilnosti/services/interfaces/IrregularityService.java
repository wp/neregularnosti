package mk.ukim.finki.wp.nepravilnosti.services.interfaces;

import jakarta.mail.MessagingException;
import mk.ukim.finki.wp.nepravilnosti.models.Irregularity;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityStatus;
import mk.ukim.finki.wp.nepravilnosti.models.enums.IrregularityType;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.IrregularityDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.ProfessorDoesNotExistException;
import mk.ukim.finki.wp.nepravilnosti.models.exceptions.RoomDoesNotExistException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IrregularityService {
    Page<Irregularity> filterIrregularities(String id, IrregularityType type, IrregularityStatus status, String sort, String username, int page, int size);
    Page<Irregularity> filterIrregularitiesForAdmin(String id, IrregularityType type, IrregularityStatus status, String sort, String username, int page, int size);
    Irregularity getIrregularityById(String id) throws IrregularityDoesNotExistException;
    void addIrregularity(String title, String description, IrregularityType type, String outsideKye, String username) throws ProfessorDoesNotExistException, RoomDoesNotExistException, MessagingException;
    void editIrregularity(String id, String title, String description, IrregularityType type, String outsideKye);
    void deleteIrregularity(String id);
    void changeInitialStatus(String id, IrregularityStatus status, String username);
    void addCommentToThread(String id, String comment, String username);
    void changeStatus(String id, String username) throws MessagingException;
    boolean isAdministration(String username);
    List<Irregularity> getUnresolvedIrregularitiesOlderThanWeek();
    List<Irregularity> findAllIrregularitiesForUser(String username);
}
