package mk.ukim.finki.wp.nepravilnosti.models.enums;

public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}

