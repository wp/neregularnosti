package mk.ukim.finki.wp.nepravilnosti.repositories;

import mk.ukim.finki.wp.nepravilnosti.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, String> {
    @Query("select r.name from Room r")
    List<String> getRoomNames();
}
