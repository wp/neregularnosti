let cache = {
    rooms: null,
    students: null,
    professors: null,
    subjects: null,
    administrations: null,
    general: null
};

const populateSearchFields = (selectElement, selectedValue = null)=> {
    console.log("vleze vo funkcijata")
    // console.log(selectElement)
    // console.log(selectedValue)
    if (!selectElement) return;

    let selectedType = selectElement.value.toLowerCase();

    if (selectedType) {
        if (cache[selectedType]) {
            updateOptionsSelect(selectElement, cache[selectedType], selectedType, selectedValue);
        } else {
            fetch('/api/' + selectedType + 's')
                .then(response => response.json())
                .then(data => {
                    cache[selectedType] = data;
                    updateOptionsSelect(selectElement, data, selectedType, selectedValue);
                })
                .catch(error => {
                    console.error('Error fetching data:', error);
                });
        }
    } else {
        let optionsSelect = selectElement.closest("form").querySelector(".optionsSelect");
        optionsSelect.setAttribute("disabled", "disabled");
        optionsSelect.innerHTML = '<option disabled value="">Одберете тип</option>';
    }
}

const updateOptionsSelect = (selectElement, data, selectedType, selectedValue) =>{
    let form = selectElement.closest("form");
    let optionsSelect = form.querySelector(".optionsSelect");

    optionsSelect.removeAttribute("disabled");


    optionsSelect.innerHTML = '<option value="">Изберете опција</option>';

    data.forEach(option => {
        let optionElement = document.createElement("option");
        if (selectedType === 'room' || selectedType === 'general') {
            optionElement.value = option;
            optionElement.textContent = option;
        } else {
            optionElement.value = option.id;
            optionElement.textContent = option.name;
        }

        if (selectedValue && optionElement.value === selectedValue) {
            optionElement.selected = true;
        }

        optionsSelect.appendChild(optionElement);
    });
}