function formatDateString(dateString) {
    return dateString.split('T')[0];
}

document.addEventListener('DOMContentLoaded', function() {
    const dateCells = document.querySelectorAll('.date');
    dateCells.forEach(function(dateCell) {
        if (dateCell.innerText) {
            const currentDateStr = dateCell.innerText;
            dateCell.innerText = formatDateString(currentDateStr);
        }
    });
});
