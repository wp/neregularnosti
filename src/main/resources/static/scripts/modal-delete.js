document.addEventListener("DOMContentLoaded", () => {
    const openModalButtons = document.querySelectorAll("[data-bs-toggle='modal']");
    const modal = document.getElementById("deleteModal");
    const modalContent = document.getElementById("modalContent");
    const deleteForm = document.getElementById("deleteForm");

    openModalButtons.forEach(button => {
        button.addEventListener("click", () => {
            const id = button.getAttribute("data-id");
            const title = button.getAttribute("data-title");
            modalContent.innerText = "Дали сте сигурни дека сакате да ја избришите пријавата: " + title + "?";
            deleteForm.action = "/irregularity/delete/" + id;
        });
    });
});