document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.edit-btn').forEach(button => {
        button.addEventListener('click', event => {
            const id = event.target.getAttribute('data-id');
            const visibleRow = document.getElementById(`visible-edit-${id}`);
            const editRow = document.getElementById(`edit-irregularity-${id}`);

            if (visibleRow && editRow) {
                visibleRow.style.display = 'none';

                if (visibleRow.nextElementSibling !== editRow) {
                    visibleRow.parentNode.insertBefore(editRow, visibleRow.nextElementSibling);
                }
                editRow.style.display = 'table-row';

                let typeSelect = editRow.querySelector(".irregularityType");
                let valueSelect = editRow.querySelector(".optionsSelect");
                let selectedValue = valueSelect ? valueSelect.getAttribute('data-selected-value') : null;
                populateSearchFields(typeSelect, selectedValue);
            }
        });
    });

    document.querySelectorAll('.cancel-btn').forEach(button => {
        button.addEventListener('click', event => {
            const id = event.target.getAttribute('data-id');
            const visibleRow = document.getElementById(`visible-edit-${id}`);
            const editRow = document.getElementById(`edit-irregularity-${id}`);

            if (visibleRow && editRow) {
                editRow.style.display = 'none';
                visibleRow.style.display = 'table-row';
            }
        });
    });
});